#pragma once

#include "ofMain.h"
#include "ofxHapPlayer.h"
#include "ofxJSON.h"
#include "ofSoundPlayer.h"
#include "ofxThreadedImageLoader.h"

struct MediaContent {
	std::string mime;
	std::string file;
	bool hap;
	uint64_t content_milis = 0;
	void getMime(std::string& type, std::string& container) const;
};

class ContentPlayer {
protected:
	ofRectangle mDimensions;
public:
	virtual void load(const MediaContent& content) = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual bool isDone() = 0;
	ofRectangle getDimensions();
	virtual ~ContentPlayer() {}
};
class VideoContentPlayer: public ContentPlayer {
	ofVideoPlayer& player;
	bool loop = false;
public:


	// Inherited via ContentPlayer
	virtual void load(const MediaContent & content) override;

	virtual void update() override;

	virtual bool isDone() override;

	virtual void draw() override;
	VideoContentPlayer(ofVideoPlayer& player, bool loop = false);

};

class HapContentPlayer : public ContentPlayer {
	bool loop = false;
	ofxHapPlayer& player;
public:


	// Inherited via ContentPlayer
	virtual void load(const MediaContent & content) override;

	virtual void update() override;

	virtual bool isDone() override;

	virtual void draw() override;
	HapContentPlayer(ofxHapPlayer& player, bool loop = false);
};
class ImageContentPlayer : public ContentPlayer {
	ofxThreadedImageLoader& loader;
	ofImage image;
	uint64_t timer_end = 0;
	bool timer_overflow = false;
public:
	virtual void load(const MediaContent & content) override;

	virtual void update() override;

	virtual bool isDone() override;

	virtual void draw() override;
	ImageContentPlayer(ofxThreadedImageLoader& loader);
};

class ContentPlayerFactory {
private:
	ofVideoPlayer video_player;
	ofxHapPlayer hap_player;
	ofxThreadedImageLoader loader;
	std::unique_ptr<VideoContentPlayer> makeVideoPlayer(const MediaContent& description, bool loop = false);
	std::unique_ptr<HapContentPlayer> makeHapPlayer(const MediaContent& description, bool loop = false);
	std::unique_ptr<ImageContentPlayer> makeImagePlayer(const MediaContent& description);
public:
	std::unique_ptr<ContentPlayer> makePlayer(const MediaContent& description, bool loop = false);
};

class ofApp : public ofBaseApp{
	ContentPlayerFactory players_factory;
	bool scaled = false;
	public:
		std::string cms_root;
		ofFbo fbo;
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		

		std::unique_ptr<ContentPlayer> currentPlayer;
		
		//Positioning
		ofVec2f pos1;
		ofVec2f pos2;
		ofVec2f pos3;
		ofVec2f pos4;
		int towerWidth;
		int towerHeight;
		int sectionwidth;
		int topsectionheight;
		int botomsectionheight;

		ofxJSONElement settings;
		ofxJSONElement ads_json;
		void switchContent();
		vector<MediaContent> files;
		int videoFileID;
};
