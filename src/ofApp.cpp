#include "ofApp.h"
#include <boost/filesystem.hpp>


constexpr uint64_t mins_to_milis(uint64_t mins) {
	return mins * 60 * 1000;
}
constexpr uint64_t default_milis = mins_to_milis(1); // 3 minutes
//--------------------------------------------------------------
void ofApp::setup(){
	ofLogNotice("player", "Loading ads player");
	std::string user_home = std::getenv("USERPROFILE");
	ofLogNotice("player", user_home);
	try {
		ifstream file(user_home + "\\cms_home_path.txt");
		file >> cms_root;
	}
	catch (std::exception& what) {
		ofLogError("player", "Failed to load root config");
		std::exit(-1);
	}
	settings.open(ofToDataPath("settings.json"));
	videoFileID = 0;
	ofLogNotice("player", "Pulling remote adds");
	string pythoncommand = "python " + ofToDataPath(settings["executable"].asString(), true);
	try {
		ofstream fs("out.txt");
		fs << pythoncommand;
	}
	catch (std::exception) {

	}
	cout << pythoncommand << endl;
	system(pythoncommand.c_str()); //C:\\OF\\of_v0.9.8_vs_release\\apps\\myApps\\MGMvideoPlayer\\bin\\data\\pull_ads_raw.py");
	//cout << json["videoFile"].size() << endl;

	bool parsingSuccessful = ads_json.open(settings["ads"].asString());
	if (parsingSuccessful)
	{
		cout << "founded json and parssing..." << endl;
		ads_json.isArray();
		cout << ads_json.isArray() << endl;
		for (Json::ArrayIndex i = 0; i < ads_json[0]["files"].size(); i++)
		{
			std::string location, mime;
			auto& current_entry = ads_json[0]["files"][i];
			location = current_entry["local_path"].asString();
			mime = current_entry["custom_ext"].asString();
			bool hap = false;
			if (mime == "video/quicktime" || mime == "video/hap") {
				//Need to check only for quicktime
				hap = current_entry["custom_is_hap"].asBool();
			}
			//fileNames.push_back(location);
			try{
				boost::filesystem::path fullpath = boost::filesystem::canonical(cms_root + "/" + location);
				cout << fullpath << endl;
				const std::string image_tag = "image";
				uint64_t image_time = default_milis;
				if (mime.compare(0, image_tag.size(), image_tag) == 0) {
					// We care about time for images
					try {
						if (!(current_entry["required_codecs"].empty())) {
							auto time_str = current_entry["required_codecs"][0].asString();
							std::string::size_type sz;
							image_time = mins_to_milis(std::stoi(time_str, &sz));
						}
					}
					catch (std::exception) {
						//NOM NOM
					}
				}
				files.push_back({ mime, fullpath.string() , hap,  image_time });
			}
			catch (std::exception& what) {
				ofLogError() << what.what() ;
			}
			
			
			
			
			
		}
		ofLogNotice("print") << ads_json.getRawString();

	}
	if (files.size() < 1) {
		ofLogNotice() << "No files in adset exists";
		std::exit(-2);
		return;
	}
	scaled = settings["scaled"].asBool();
	if (scaled) {
		pos1 = ofVec2f(settings["SCR1POSX"].asInt(), settings["SCR1POSY"].asInt());
		pos2 = ofVec2f(settings["SCR2POSX"].asInt(), settings["SCR2POSY"].asInt());
		pos3 = ofVec2f(settings["SCR3POSX"].asInt(), settings["SCR3POSY"].asInt());
		pos4 = ofVec2f(settings["SCR4POSX"].asInt(), settings["SCR4POSY"].asInt());
		towerWidth = settings["TOWERWIDTH"].asInt();
		towerHeight = settings["TOWERHEIGHT"].asInt();
		sectionwidth = settings["OutWidth"].asInt();
		topsectionheight = settings["TopOutHeight"].asInt();
		botomsectionheight = settings["BottomOutHeight"].asInt();
		fbo.allocate(towerWidth, towerHeight);
	}
	else {
		pos1 = ofVec2f(settings["SCR1POSX"].asInt(), settings["SCR1POSY"].asInt());
		pos2 = ofVec2f(settings["SCR2POSX"].asInt(), settings["SCR2POSY"].asInt());
		pos3 = ofVec2f(settings["SCR3POSX"].asInt(), settings["SCR3POSY"].asInt());
		pos4 = ofVec2f(settings["SCR4POSX"].asInt(), settings["SCR4POSY"].asInt());
		towerWidth = settings["TOWERWIDTH"].asInt();
		towerHeight = settings["TOWERHEIGHT"].asInt();
		sectionwidth = settings["OutWidth"].asInt();
		topsectionheight = settings["TopOutHeight"].asInt();
		botomsectionheight = settings["BottomOutHeight"].asInt();
		fbo.allocate(towerWidth / 4, towerHeight);
	}
	MediaContent content = files[videoFileID];
	if (files.size() == 1) {
		currentPlayer = players_factory.makePlayer(content, true);
	}
	else {
		currentPlayer = players_factory.makePlayer(content);
	}
	
	if (currentPlayer) {
		currentPlayer->load(content);

	}

}

//--------------------------------------------------------------
void ofApp::update(){
	if (currentPlayer) {
		currentPlayer->update();
		if (currentPlayer->isDone()) {
			switchContent();
		}
	}
	/*
	if (!ishap) {
		if (videoPlayer.getIsMovieDone()) {
			changeVideo();
			if (isaudio)
				audio.play();
		}
	}
	else {
		if (hapPlayer.getIsMovieDone()) {
			changeVideoHap();
			if (isaudio)
				audio.play();
		}
	
	}
	*/
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(0);
	if (currentPlayer) {
		ofPushMatrix();
		ofRectangle dims = currentPlayer->getDimensions();
		float x, y;
		if (!scaled) {
			fbo.begin();
			ofClear(0, 0, 0, 0);
			x = fbo.getWidth() / dims.width;
			y = fbo.getHeight() / dims.height;
			ofScale(x, y);
			currentPlayer->draw();
			fbo.end();
			ofBackground(0);
			fbo.getTextureReference().drawSubsection(pos1.x, pos1.y, sectionwidth/2, topsectionheight, 0, 0);
			fbo.getTextureReference().drawSubsection(pos3.x, pos3.y, sectionwidth/2, botomsectionheight, 0, topsectionheight);
		}
		else {
			fbo.begin();
			ofClear(0, 0, 0, 0);
			x = fbo.getWidth() / dims.width;
			y = fbo.getHeight() / dims.height;
			ofScale(x, y);
			currentPlayer->draw();
			fbo.end();
			ofBackground(0);
			//	videoFbo.draw(0, 0);
			fbo.getTextureReference().drawSubsection(pos1.x, pos1.y, sectionwidth, topsectionheight, 0, 0);
			fbo.getTextureReference().drawSubsection(pos2.x, pos2.y, sectionwidth, topsectionheight, sectionwidth, 0);
			fbo.getTextureReference().drawSubsection(pos3.x, pos3.y, sectionwidth, botomsectionheight, 0, topsectionheight);
			fbo.getTextureReference().drawSubsection(pos4.x, pos4.y, sectionwidth, botomsectionheight, sectionwidth, topsectionheight);
		}
		
		ofPopMatrix();
	}
	
}
void ofApp::switchContent()
{
	videoFileID++;
	cout << "changevideo" << endl;
	if (videoFileID > files.size() - 1) {
		videoFileID = 0;
	}
	MediaContent& content = files[videoFileID];
	currentPlayer = players_factory.makePlayer(content);
	if (currentPlayer) {
		currentPlayer->load(content);
	}
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void MediaContent::getMime(std::string & type, std::string & container) const
{
	auto parsed_mime = ofSplitString(mime, "/");
	if (parsed_mime.size() != 2) {
		type = "";
		container = "";
		return;
	}
	type = parsed_mime[0];
	container = parsed_mime[1];
}

void VideoContentPlayer::load(const MediaContent & content)
{
	bool loaded = player.load(content.file);
	if (!loaded) {
		ofLogNotice("player","not loaded %s ", content.file.c_str());
	}
	if (loop) {
		player.setLoopState(OF_LOOP_NORMAL);
	}
	else {
		player.setLoopState(OF_LOOP_NONE);
	}
	player.play();
	mDimensions = ofRectangle(0, 0, player.getWidth(), player.getHeight());
}

void VideoContentPlayer::update()
{
	player.update();
}

bool VideoContentPlayer::isDone()
{
	return player.getIsMovieDone();
}

void VideoContentPlayer::draw()
{
	player.draw(0, 0);
}

VideoContentPlayer::VideoContentPlayer(ofVideoPlayer & player_, bool loop_)
	:player(player_), loop(loop_)
{
}

void HapContentPlayer::load(const MediaContent & content)
{
	bool loaded = player.load(content.file);
	if (!loaded) {
		ofLogNotice("player", "hap not loaded %s ", content.file.c_str());
	}
	if (loop) {
		player.setLoopState(OF_LOOP_NORMAL);
	}
	else {
		player.setLoopState(OF_LOOP_NONE);
	}
	
	player.play();
	mDimensions = ofRectangle(0, 0, player.getWidth(), player.getHeight());
}

void HapContentPlayer::update()
{
	player.update();
}

bool HapContentPlayer::isDone()
{
	return player.getIsMovieDone();
}

void HapContentPlayer::draw()
{
	
	player.getTexture()->draw(0,0);
}

HapContentPlayer::HapContentPlayer(ofxHapPlayer & player_, bool loop_)
	:player(player_), loop(loop_)
{
}

void ImageContentPlayer::load(const MediaContent & content)
{
	loader.loadFromDisk(image, content.file);
	timer_overflow = false;
	timer_end = ofGetElapsedTimeMillis() + content.content_milis;
}

void ImageContentPlayer::update()
{
	mDimensions = ofRectangle(0, 0, image.getWidth(), image.getHeight());
	if (ofGetElapsedTimeMillis() > timer_end) {
		timer_overflow = true;
	}
}

bool ImageContentPlayer::isDone()
{
	return timer_overflow;
}

void ImageContentPlayer::draw()
{
	image.draw(0, 0);
}

ImageContentPlayer::ImageContentPlayer(ofxThreadedImageLoader & player)
	:loader(player)
{
}

std::unique_ptr<VideoContentPlayer> ContentPlayerFactory::makeVideoPlayer(const MediaContent & description, bool loop)
{
	return std::make_unique<VideoContentPlayer>(video_player, loop);
}

std::unique_ptr<HapContentPlayer> ContentPlayerFactory::makeHapPlayer(const MediaContent & description, bool loop)
{
	return std::make_unique<HapContentPlayer>(hap_player, loop);
}

std::unique_ptr<ImageContentPlayer> ContentPlayerFactory::makeImagePlayer(const MediaContent & description)
{

	return std::make_unique<ImageContentPlayer>(loader);
}

std::unique_ptr<ContentPlayer> ContentPlayerFactory::makePlayer(const MediaContent & description, bool loop)
{
	std::string type, content;
	description.getMime(type, content);
	if (type == "video") {
		if (description.hap) {
			return makeHapPlayer(description, loop);
		}
		return makeVideoPlayer(description, loop);
	}
	if (type == "image") {
		return makeImagePlayer(description);
	}
	return nullptr;
}

ofRectangle ContentPlayer::getDimensions()
{
	return mDimensions;
}
