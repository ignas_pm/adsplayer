#include "ofMain.h"
#include "ofApp.h"

//========================================================================

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32)
#include <Windows.h>
    //int main(int argc, char* argv[]) {
	int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {
#else
	int main(int argc, char* argv[]) {
#endif


//	ofSetupOpenGL(1024,768,OF_WINDOW);			// <-------- setup the GL context
	ofGLFWWindowSettings settings;
	settings.setGLVersion(4, 4);
	settings.width = 1920-1;
	settings.height = 1080;
	//settings.multiMonitorFullScreen = true;
	ofCreateWindow(settings);
	//ofAppGLFWWindow win;
	//win.setMultiDisplayFullscreen(true); //this makes the fullscreen window span across all your monitors  



	ofRunApp(new ofApp());

}
