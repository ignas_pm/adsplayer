import requests
import logging
import sys
import json
import mimetypes
import subprocess
import copy
import os.path
import os
import datetime

def get_cms_root():
    home = os.environ["USERPROFILE"]
    with open(home + "\\cms_home_path.txt") as f:
        cms_root = f.read()
        return cms_root

class Stream(object):
    def __init__(self):
        self.kin = {}

def parse_streams(txt_input):
    try:
        streams = []
        curr_stream = None
        for line in txt_input.splitlines():
            if line == "[STREAM]":
                curr_stream = Stream()
            elif line == "[/STREAM]":
                streams.append(copy.deepcopy(curr_stream))
            else:
                rest = line.split("=")
                curr_stream.kin[rest[0]] = rest[1] 
        return streams
    except Exception as ex:
        print("What{}".format(ex))

def search_video_stream(streams):
    for stream in streams:
        if stream.kin["codec_type"] == "video":
            return stream

def run_media_probe(filename):
    proc = subprocess.Popen(["ffprobe.exe", "-show_streams", filename], stdout=subprocess.PIPE)
    output = ""
    while True:
        line = proc.stdout.readline()
        if line != '':
            output += line
        else:
            break
    return output

def is_hap(file, mime):
    out = run_media_probe(file)
    streams = parse_streams(out)
    stream = search_video_stream(streams)
    codec = stream.kin["codec_name"]
    if codec == "hap":
        return True
    return False




logging.basicConfig(level=logging.DEBUG)
CMS_API = "http://towercms.505.lt/api/ad-set"
API_KEY = "c52f21e51aaf43c6adc02263026f8ccb"
FILE = "ads.json"
SETTINGS_FILE = "settings.json"

def main():

    out = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    json_config = None
    mimetypes.add_type("video/hap", ".hap")
    with open(SETTINGS_FILE) as settings:
        json_config = json.load(settings)
    cms_root = get_cms_root()
    current_time = datetime.datetime.now().strftime('%H:%M:%S')
    ads_filter = json_config["adsfilter"]
    try:
        response = requests.get(CMS_API, {"api_key":API_KEY, "time":current_time,"type": ads_filter})
        print(response.status_code)
        if not response.ok:
            logging.error("Failed to download file: {}".format(response.reason))
            return
        entries = json.loads(response.text)
        for entry in entries:
            for file_descr in entry["files"]:
                path = os.path.abspath(cms_root + "\\" + file_descr["local_path"])
                print("Loading path: {}".format(path))
                file_descr["custom_ext"] = mimetypes.guess_type(path)[0]
                mime_type = file_descr["custom_ext"].split("/")
                print(mime_type)
                if mime_type[0] != "video":
                    # No problem here No need to find hap
                    continue
                file_descr["custom_is_hap"] = is_hap(path, file_descr["custom_ext"])
                print("Hap: {}".format(file_descr["custom_is_hap"]))
            
        with open(FILE, 'w') as f:
            json.dump(entries, f)
    except Exception as ex:
        logging.error("Failed to download adds file:{}".format(ex))
    #Restoring path
    os.chdir(out)

if __name__ == '__main__':
    main()